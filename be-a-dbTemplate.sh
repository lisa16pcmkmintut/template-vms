hostnamectl set-hostname dbTemplate.pcmk.local

yum -y install postgresql94-server

/usr/pgsql-9.4/bin/postgresql94-setup initdb

cd ~/lisa16pcmkmintut/demo-cluster-build/postgresql
git pull
find . -type f -print0 | cpio -pmud0 /

cd ~postgres
chown -R postgres:postgres ./
chmod 0600 .pgpass
chmod 0644 .pgsql_profile .bash_profile
chmod 0744 sbin/enslave-this-node.sh

su - postgres
mkdir -p log

pg_ctl start
psql -c "CREATE USER replicator REPLICATION LOGIN ENCRYPTED PASSWORD 'snakeOil782-8';

exit


