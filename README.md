# README #

### What is this repository for? ###

The repo holds instructions and scripts for creating the template
VMS you'll need to clone to create the VMs for the QuickStart to
Pacemaker HA mini-tuturial at LISA16.

### How do I get set up? ###

Instructions based on using Oracle VM VirtualBox Manager.

This is NOT for use in real life. At the least, for real use
you should create separate filesystems for /tmp, /var, /home
and possibly /opt, /var/atlasssian, /var/lib/pgsql, etc.

### Host only networking ###
We're going to create a set of VMs that can talk to each other and
to the host - your laptop. And they can also talk out to the
world, but the world can't talk to them.

I used network 192.168.56.0/24 when I did this work. So my
scripts assume. So, either make your host-only network use that
IP range or adapt the scripts to fit your range.

### Create a VM named appTemplate ###
* Type: Linux Red Hat (64-bit)
* two cores, 2.0 GB RAM
* Two network interfaces
    * connect adaptor 1 to a NAT network
    * connect adaptor 2 to a Host-only Adapter
* one 8 GB virtual disk
* Install CentOS 7.2 minimal (get a DVD image from 
    * accept defaults for everything
    * set the root password to rootword
    * after first reboot,
    * log in and do this:
~~~~
yum -y install git
mkdir lisa16pcmkmintut
cd lisa16pcmkmintut
git clone https://bitbucket.org/lisa16pcmkmintut/template-vms.git
cd template-vms
bash prepare-to-be-a-template.sh
shutdown -P now
~~~~
### Clone this VM into dbTemplate ###
1. Use your VirtualBox manager or whatever you have to duplicate this VM. Name the new one dbTemplate.
2. When you're done, start appTemplate again, login and we'll continue to customize it.
### Finish customizing the appTemplate ###
~~~~
cd lisa16pcmkmintut/template-vms
bash be-an-app-template.sh
shutdown -P now
~~~~
### Finish customizing the dbTemplate ###
~~~~
cd lisa16pcmkmintut/template-vms
bash be-a-db-template.sh
shutdown -P now
~~~~
### Clone your templates into servers ###
1. Clone your dbTemplate into "pam" (db1) and "pat" (db2)
1. Clone your appTemplate into "amy" (app1), "ann" (app2), and "ari" (app3"
3. Start each one, log in, and set it's hostname and fix it's IP address with this:
~~~~
# Set VMNAME here accordingly, pasting the rest should work.
VMNAME=amy # ann ari pam pat
hostnamectl set-hostname $VMNAME.pcmk.local
IPA=($(getent hosts $VMNAME))
sed -i -e "/^IPADDR=192.*$/s//IPADDR=${IPA}/" /etc/sysconfig/network-scripts/ifcfg-enp0s8
# Then to make sure it comes up properly:
reboot
~~~~
Log in to each and check with "ip address" that they each have the right addresses
### You're done for now. ###
