
# Start your NAT network interface.
#   You should start the interface that's connected
#   to your NAT network so you can get out of the host
#   and install stuff.
#
# Here are some likely inteface names for you
#
ifup eno16780032
ifup enp0s3
ifup etho

# For expedience, disable security. I know, I know.  Don't say it.
systemctl disable firewalld
systemctl stop    firewalld
iptables --flush
sed -i '/^SELINUX=/s/=.*$/=disabled/' /etc/selinux/config
setenforce 0

# Get rid of what we're about to add before we add
# it so we don't duplicate stuff
sed -r -i '/192\.168\.56\.[0-9]+ .*\.pcmk\.local/d' /etc/hosts

# Put in a standard set of host file entries.
#   The rest of the demo and workshop setup depends on having
#   this set of IP addresses and names.
cat <<EOM >> /etc/hosts
#
192.168.56.220 bareTemplate.pcmk.local bareTemplate
192.168.56.221 appTemplate.pcmk.local appTemplate
192.168.56.222 dbTemplate.pcmk.local dbTemplate
#
192.168.56.201 amy.pcmk.local amy app1
192.168.56.202 ann.pcmk.local ann app2
192.168.56.203 ari.pcmk.local ari app3
192.168.56.204 pam.pcmk.local pam db1
192.168.56.205 pat.pcmk.local pat db2
#
192.168.56.251 demo-jira.pcmk.local       demo-jira
192.168.56.252 demo-confluence.pcmk.local demo-confluence
192.168.56.253 demo-db.pcmk.local         demo-db
192.168.56.254 demo-db-rep.pcmk.local     demo-db-rep
EOM

# Static network addressing for Pacemaker nodes
#
hostnamectl set-hostname bareTemplate

cat <<EOM > /etc/sysconfig/network-scripts/ifcfg-enp0s8
DEVICE=enp0s8
BOOTPROTO=none
ONBOOT=yes
PREFIX=24
IPADDR=192.168.56.220
EOM

# Packages we really don't need on a server, especially a VM.
#
yum -y remove iwl* avahi* alsa*

# Install packages to add the EPEL, EL, and PostgreSQL 9.4 repos
yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum -y install http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
yum -y install https://download.postgresql.org/pub/repos/yum/9.4/redhat/rhel-7-x86_64/pgdg-centos94-9.4-3.noarch.rpm

# Update to include the new repos *and* to get
# updates for everything that's already installed.
yum -y update

# Packages for generic admin work
yum -y install vim screen rsync lsyncd wget curl bind-utils lsof

# Packages for life with Pacemaker
yum -y install pacemaker pcs resource-agents fence-agents-common

# Set the password for the hacluster user - it was installed with pacemaker
echo NewBlue39 | passwd --stdin hacluster

# enable pcs to start on boot up, no need to start it now
systemctl enable pcsd

# Return to the dir we started in
popd
